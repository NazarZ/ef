﻿using Binary.Linq.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Structure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueriesController : ControllerBase
    {
        private readonly IQueryService _queryService;
        public QueriesController(IQueryService service)
        {
            _queryService = service;
        }
        // GET: api/Projects
        [HttpGet("GetCountTasksUserInProjects/{id}")]
        public ActionResult<Dictionary<ProjectModel, int>> GetCountTasksUserInProjects(string id)
        {
            return Ok(_queryService.GetCountTasksUserInProjects(Convert.ToInt32(id)));
        }
        [HttpGet("GetTasksByUserId/{id}")]
        public ActionResult<List<TaskModel>> GetTasksByUserId(string id)
        {
            var response = _queryService.GetTasksByUserId(Convert.ToInt32(id));
            return Ok(response);
        }
        [HttpGet("GetFinishedTaskByUserId/{id}")]
        public ActionResult<List<FinishedTaskModel>> GetFinishedTaskByUserId(string id)
        {
            return Ok(_queryService.GetFinishedTaskByUserId(Convert.ToInt32(id)));
        }
        [HttpGet("GetTeams")]
        public ActionResult<List<TeamUsers>> GetTeams()
        {
            return Ok(_queryService.GetTeams());
        }
        [HttpGet("GetUserOrderByNameWithTask")]
        public ActionResult<List<UserWithTaskModel>> GetUserOrderByNameWithTask()
        {
            return Ok(_queryService.GetUserOrderByNameWithTask());
        }
        [HttpGet("GetTask6/{id}")]
        public ActionResult<Task6Model> GetTask6(string id)
        {
            return Ok(_queryService.GetTask6(Convert.ToInt32(id)));
        }




    }
}
