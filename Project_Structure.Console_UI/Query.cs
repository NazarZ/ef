﻿using Binary.Linq.BL.Models;
using Binary.Linq.BL.Models.Enum;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Project_Structure.Console_UI
{
    public class Query
    {
        private Client _client;
        public Query()
        {
            _client = new();
        }
        //1. Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків).
        public async void GetCountTasksUserInProjects(int userId)
        {
            var tasks = await _client.GetCountTasksUserInProjects(userId);
            if(tasks is not null)
            {
                foreach (var task in tasks)
                {
                    Console.WriteLine($"{task.Key.Project.Name} --- {task.Value}");
                }
            }
        }

        //2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків).
        public async void GetTasksByUserId(int userId)
        {
            var tasks = await _client.GetTasksByUserId(userId);
            if(tasks is not null)
            {
                foreach (var task in tasks)
                {
                    Console.WriteLine($"Task: {task.Task.Name}  ---  Description: {task.Task.Description}");
                }
            }
        }

        //3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2021) році 
        //для конкретного користувача (по id).
        public async void GetFinishedTaskByUserId(int userId)
        {
            var tasks = await _client.GetFinishedTaskByUserId(userId);

            if(tasks is not null)
            {
                foreach (var task in tasks)
                {
                    Console.WriteLine($"ID: {task.Id}  ---  Name: {task.Name}");
                }
            }
        }

        //4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років,
        //відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.
        //P.S -в цьому запиті допускається перевірка лише року народження користувача, без прив'язки до місяця/дня/часу народження.
        public async void GetTeams()
        {
            var teams =  await _client.GetTeams();
            if(teams is not null)
            {
                foreach (var team in teams)
                {
                    Console.WriteLine($"ID: {team.Id}  ---  Name: {team.TeamName}\nUsers:\n");
                    foreach (var user in team.Users)
                    {
                        Console.WriteLine($"{user.FirstName} {user.LastName} {user.Email}");
                    }
                    Console.WriteLine("\n");
                }
            }
        }

        //5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks
        //по довжині name (за спаданням).
        public async void GetUserOrderByNameWithTask()
        {
            var users = await _client.GetUserOrderByNameWithTask();

            if(users is not null)
            {
                foreach (var user in users)
                {
                    Console.WriteLine($"{user.User.FirstName} {user.User.LastName} {user.User.Email}\nTasks:");
                    foreach (var task in user.Tasks)
                    {
                        Console.WriteLine($"{task.Name}  ---  {task.Description}");
                    }
                    Console.WriteLine("\n");
                }
            }
        }

        //6. Отримати наступну структуру (передати Id користувача в параметри):
        //  User
        // Останній проект користувача(за датою створення)
        // Загальна кількість тасків під останнім проектом
        // Загальна кількість незавершених або скасованих тасків для користувача
        // Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
        //P.S. - в даному випадку, статус таска не має значення, фільтруємо тільки за датою.
        public async void GetTask6(int userId)
        {

            var str = await _client.GetTask6(userId);
            if(str is not null)
            {
                Console.WriteLine($"{str.User.FirstName} {str.User.FirstName}");
                Console.WriteLine($"Last project name: {str.LastProjectUser.Name}  Description: {str.LastProjectUser.Description}");
                Console.WriteLine($"Long task: {str.LongestUserTask.Name}");
                Console.WriteLine($"Total unfinished task: {str.TotalCountUnfinishedTask}");
                Console.WriteLine($"Total task in last project: {str.TotalCountUnfinishedTask}");
            }
        }
    }
}
