﻿using Project_Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Project_Structure.BLL.Models
{
    public class UserWithTaskModel
    {
        public User User { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
