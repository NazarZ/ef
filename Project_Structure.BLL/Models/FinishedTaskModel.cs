﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Models
{
    public class FinishedTaskModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
