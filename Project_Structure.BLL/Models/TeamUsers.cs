﻿using Project_Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Models
{
    public class TeamUsers
    {
        public int Id { get; set; }
        public string TeamName { get; set; }
        public List<User> Users { get; set; }
    }
}
