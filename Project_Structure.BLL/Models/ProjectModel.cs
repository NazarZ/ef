﻿using Project_Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Models
{
    public class ProjectModel
    {
        public Project Project { get; set; }
        public List<TaskModel> Tasks { get; set; }
        public User Author { get; set; }
        public Team Team { get; set; }
    }
}
