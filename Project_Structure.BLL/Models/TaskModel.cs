﻿using Project_Structure.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Structure.BLL.Models
{
    public class TaskModel
    {
        public Task Task { get; set; }
        public User Performer { get; set; }
    }
}
