﻿using AutoMapper;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services.Abstract
{
    public abstract class BaseService
    {
        private protected readonly IUnitOfWork _context;
        private protected readonly IMapper _mapper;

        public BaseService(IUnitOfWork context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
    }
}
