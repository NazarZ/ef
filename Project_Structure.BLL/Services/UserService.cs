﻿using AutoMapper;
using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.BLL.Services.Abstract;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Repositories;
using System.Collections.Generic;

namespace Project_Structure.BLL.Services
{
    public class UserService : BaseService, IUserService
    {
        public UserService(IUnitOfWork context, IMapper mapper)
            : base(context, mapper)
        {
        }
        public void CreateUser(UserDTO user)
        {
            var userEntity = _mapper.Map<User>(user);
            _context.Users.Create(userEntity);
            _context.Save();
        }

        public void DeleteUser(int idTask)
        {
            _context.Users.Delete(idTask);
            _context.Save();
        }

        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _mapper.Map<IEnumerable<UserDTO>>(_context.Users.GetAll());
        }

        public UserDTO GetUserById(int id)
        {
            return _mapper.Map<UserDTO>(_context.Users.Get(id));
        }

        public void UpdateUser(UserDTO user)
        {
            var userEntity = _mapper.Map<User>(user);
            _context.Users.Update(userEntity);
            _context.Save();
        }
    }
}
