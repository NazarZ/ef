﻿using Project_Structure.BLL.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IProjectService : IDisposable
    {
        public IEnumerable<ProjectDTO> GetAllProjects();
        public ProjectDTO GetProjectById(int id);
        public void CreateProject(ProjectDTO project);
        public void UpdateProject(ProjectDTO project);
        public void DeleteProject(int idProject);
    }
}
