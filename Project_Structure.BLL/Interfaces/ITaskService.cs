﻿using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using Project_Structure.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITaskService
    {
        public IEnumerable<TaskDTO> GetAllTasks();
        public TaskDTO GetTaskById(int id);
        public void CreateTask(TaskDTO task);
        public void UpdateTask(TaskDTO task);
        public void DeleteTask(int idTask);
    }
}
