﻿using Project_Structure.BLL.DTOs;
using Project_Structure.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface ITeamService
    {
        public IEnumerable<TeamDTO> GetAllTeams();
        public TeamDTO GetTeamById(int id);
        public void CreateTeam(TeamDTO team);
        public void UpdateTeam(TeamDTO team);
        public void DeleteTeam(int idTask);
    }
}
