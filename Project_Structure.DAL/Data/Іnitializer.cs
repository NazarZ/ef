﻿using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;

namespace Project_Structure.DAL.Data
{
    public static class Іnitializer
    {
        static JsonSerializerOptions options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true
        };
        public static List<Project> ProjectInit()
        {
            var json = System.IO.File.ReadAllText($@"{Directory.GetCurrentDirectory()}\..\Project_Structure.DAL\Data\Projects.json");
            return JsonSerializer.Deserialize<List<Project>>(json, options);
        }
        public static List<Task> TaskInit()
        {
            var json = System.IO.File.ReadAllText($@"{Directory.GetCurrentDirectory()}\..\Project_Structure.DAL\Data\Tasks.json");
            return JsonSerializer.Deserialize<List<Task>>(json, options);
        }
        public static List<Team> TeamInit()
        {
            var json = System.IO.File.ReadAllText($@"{Directory.GetCurrentDirectory()}\..\Project_Structure.DAL\Data\Teams.json");
            return JsonSerializer.Deserialize<List<Team>>(json, options);
        }
        public static List<User> UserInit()
        {
            var json = System.IO.File.ReadAllText($@"{Directory.GetCurrentDirectory()}\..\Project_Structure.DAL\Data\Users.json");
            return JsonSerializer.Deserialize<List<User>>(json, options);
        }
        public static void EFInit(IUnitOfWork db)
        {
            foreach(var project in ProjectInit())
            {
                db.Projects.Create(project);
            }
            foreach (var task in TaskInit())
            {
                db.Tasks.Create(task);
            }
            foreach (var team in TeamInit())
            {
                db.Teams.Create(team);
            }
            foreach (var user in UserInit())
            {
                db.Users.Create(user);
            }
        }
    }
}
