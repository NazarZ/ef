﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.DAL.Repositories
{
    public class TeamRepository : BaseRepository, IRepository<Team>
    {
        public TeamRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public void Create(Team item)
        {
            item.Id = 0;
            _db.Teams.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _db.Teams.SingleOrDefault(x => x.Id == id);
            if (toRemove is not null)
            {
                _db.Teams.Remove(toRemove);
            }
        }

        public IEnumerable<Team> Find(Func<Team, bool> predicate)
        {
            return _db.Teams.Where(predicate).ToList();
        }

        public Team Get(int id)
        {
            return _db.Teams.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Team> GetAll()
        {
            return _db.Teams.ToList();
        }

        public void Update(Team item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
