﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.DAL.Repositories
{
    public class UserRepository : BaseRepository, IRepository<User>
    {
        public UserRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public void Create(User item)
        {
            item.Id = 0;
            _db.Users.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove =_db.Users.SingleOrDefault(x => x.Id == id);
            if (toRemove is not null)
            {
               _db.Users.Remove(toRemove);
            }
        }

        public IEnumerable<User> Find(Func<User, bool> predicate)
        {
            return _db.Users.Where(predicate).ToList();
        } 

        public User Get(int id)
        {
            return _db.Users.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return _db.Users.ToList();
        }

        public void Update(User item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
