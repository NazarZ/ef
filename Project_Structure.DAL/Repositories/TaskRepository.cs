﻿using Microsoft.EntityFrameworkCore;
using Project_Structure.DAL.Context;
using Project_Structure.DAL.Entities;
using Project_Structure.DAL.Interfaces;
using Project_Structure.DAL.Interfaces.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Project_Structure.DAL.Repositories
{
    public class TaskRepository : BaseRepository, IRepository<Task>
    {
        public TaskRepository(ProjectDbContext context)
            : base(context)
        {
        }

        public void Create(Task item)
        {
            item.Id = 0;
            _db.Tasks.Add(item);
        }

        public void Delete(int id)
        {
            var toRemove = _db.Tasks.SingleOrDefault(x => x.Id == id);
            if (toRemove is not null)
            {
                _db.Tasks.Remove(toRemove);
            }
        }

        public IEnumerable<Task> Find(Func<Task, bool> predicate)
        {
            return _db.Tasks.Where(predicate).ToList();
        }

        public Task Get(int id)
        {
            return _db.Tasks.SingleOrDefault(x => x.Id == id);
        }

        public IEnumerable<Task> GetAll()
        {
            return _db.Tasks.ToList();
        }

        public void Update(Task item)
        {
            _db.Entry(item).State = EntityState.Modified;
        }
        public void Dispose()
        {
            _db.Dispose();
        }
    }
}
