﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.DAL.Entities
{
    public class Project : BaseEntity
    {
        [MaxLength(200)]
        public string? Name { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        public DateTime Deadline { get; set; }
        public DateTime CreatedAt { get; set; }

        public int? TeamId { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }

        public int? AuthorId { get; set; }
        [JsonIgnore]
        public User Author { get; set; }
        [JsonIgnore]
        public ICollection<Task> Tasks { get; set; }

        public Project()
        {
            Tasks = new List<Task>();
        }
    }
}
