﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.DAL.Entities
{
    public class Team : BaseEntity
    {
        [MaxLength(300)]
        public string? Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
