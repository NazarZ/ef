﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.DAL.Entities
{
    public class User : BaseEntity
    {

        [MaxLength(20)]
        public string? FirstName { get; set; }
        [MaxLength(20)]
        public string? LastName { get; set; }
        [DataType(DataType.EmailAddress)]
        [EmailAddress]
        public string? Email { get; set; }
        public DateTime RegisteredAt { get; set; }
        public DateTime BirthDay { get; set; }

        public int? TeamId { get; set; }
        [JsonIgnore]
        public Team Team { get; set; }
    }
}
