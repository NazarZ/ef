﻿using Binary.Linq.BL.Models.Enum;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace Project_Structure.DAL.Entities
{
    public class Task : BaseEntity
    {
        [MaxLength(300)]
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TaskState State { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime? FinishedAt { get; set; }

        public int ProjectId { get; set; }
        [JsonIgnore]
        public Project Project { get; set; }

        public int? PerformerId { get; set; }
        [JsonIgnore]
        public User Performer { get; set; }
    }
}
