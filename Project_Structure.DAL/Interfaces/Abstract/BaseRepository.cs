﻿using Project_Structure.DAL.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.DAL.Interfaces.Abstract
{
    public abstract class BaseRepository
    {
        protected readonly ProjectDbContext _db;
        public BaseRepository(ProjectDbContext context)
        {
            _db = context;
        }
    }
}
